-- SUMMARY --

Every core update rewrites any changes we have done in robots.txt file.
This module allows to place all custom changes in database and append them
to robots.txt file when saving the module settings or during cron.
It means that even after replacing your robots.txt with a default one,
following cron  will append your changes again.

-- REQUIREMENTS --

None.

-- INSTALLATION --

Install as usual, see http://drupal.org/node/895232 for further information

-- CONFIGURATION --

* Set permission "Administer custom robots module" to proper roles if needed.
* Add your custom data on page admin/config/search/custom_robots

-- CONTACT --

Current maintainers:
* Aleksey Zubenko (Alex Zu) - https://www.drupal.org/user/1797134
