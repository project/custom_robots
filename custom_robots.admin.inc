<?php

/**
 * @file
 * Administrative page callbacks for the custom_robots module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function custom_robots_admin_settings() {
  $file_content = _custom_robots_get_robots_data();
  $default_content = explode(CUSTOM_ROBOTS_PREFIX_TEXT, $file_content);
  $form['robots_default_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default robots.txt text'),
    '#description' => t('This data goes along to default Drupal installation.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Shows default robots.txt text.
  $form['robots_default_wrapper']['robots_default'] = array(
    '#type' => 'markup',
    '#markup' => check_markup($default_content[0]),
  );
  $form['custom_robots_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Extra contents of robots.txt'),
    '#description' => t('This data will be appended to the default robots.txt text and even if being overwritten while core update, it will be restored after following cron.'),
    '#collapsible' => FALSE,
  );
  // Shows custom robots.txt text, appended to the default one.
  $form['custom_robots_wrapper']['custom_robots'] = array(
    '#type' => 'textarea',
    '#title' => t('Extra contents of robots.txt'),
    '#description' => t("Here you can add custom strings to robots.txt file. They wouldn't be replaced during core update"),
    '#default_value' => variable_get('custom_robots', ''),
    '#cols' => 60,
    '#rows' => 20,
    '#wysiwyg' => FALSE,
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Save custom robots.txt data and rewrite robots.txt file.
 */
function custom_robots_admin_settings_submit($form, &$form_state) {
  $data = $form_state['values']['custom_robots'];
  variable_set('custom_robots', $data);
  $robots_data = _custom_robots_get_robots_data();
  $robots_data = explode(CUSTOM_ROBOTS_PREFIX_TEXT, $robots_data);
  $default_robots = $robots_data[0];
  _custom_robots_file_overwrite($data, $default_robots);
}

/**
 * Get current robots.txt filecontent.
 */
function _custom_robots_get_robots_data() {
  $files = array(
    DRUPAL_ROOT . '/robots.txt',
    DRUPAL_ROOT . '/sites/default/default.robots.txt',
  );
  foreach ($files as $file) {
    if (file_exists($file) && is_readable($file)) {
      $file_content = file_get_contents($file);
      break;
    }
  }
  return $file_content;
}
